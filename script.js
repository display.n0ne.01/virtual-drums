function playSound(e){
    //console.log(e.keyCode);
    let key = document.querySelector(`.key[data-key = "${e.keyCode}"]`);
    key.classList.add("playing");
    // when to remove class 'playing'? simple way is to set the interval:

         setInterval(function () {
           key.classList.remove("playing")
         }, 200);

         
    let audio = document.querySelector(`audio[data-key = "${e.keyCode}"]`);
    if (!audio) return ;
    // rewind audio to start
    audio.currentTime = 0;
    audio.play();
}

window.addEventListener('keydown', playSound);

